# Lorem Picsum (picsum.phtos) TypeScript client

![Testing Status Badge](https://gitlab.com/ap3k/node_modules/picsum-photos/badges/master/pipeline.svg/)

> Also supports random images in original resuliton

NPM package: <https://www.npmjs.com/package/picsum-photos>

Git repo: <https://gitlab.com/ap3k/node_modules/picsum-photos>

Lorem-picsum: <https://picsum.photos/>

## Installation

With NPM

```bash
npm install picsum-photos
```

With Yarn

```bash
yarn add picsum-photos
```

## Usage

> Read the Source luke!

### Get random image in original size

```ts
// Require it in
const { Picsum } = require('picsum-photos')

const image = await Picsum.random()
// image
interface ImageObject {
  id: number;            // Image ID in Lorem Picsum
  author: string;        // Image author
  width: number;         // Image width
  height: number;        // Image height
  url: string;           // Image original URL (source site)
  download_url: string;  // Image direct URL for download
}
```

### Get image with certain properties

```ts
// Require it in
const { Picsum } = require('picsum-photos')


// Possible config values
interface PicsumConfig {
  blur?: number;         // Level of image blurriness form 0-10 (0)
  cache?: boolean;       // Allow browser image cache, (true)
  grayscale?: boolean;   // Image grayscale or normal, (false)
  height?: number;       // Image height (200)
  id?: number;           // Get certain image with given ID (0)
  jpg?: boolean;         // Get image url as .jpg (false)
  width?: number;        // Image width (200)
}


Picsum.url()
// https://picsum.photos/200/200

// Overwrite defaults
Picsum.url({
  blur: 2
  cache: false
  grayscale: true
  height: 200
  id: 30
  jpg: true
  width: 300
})
// https://picsum.photos/id/30/200/300.jpg?grayscale=true&blur=2

```
